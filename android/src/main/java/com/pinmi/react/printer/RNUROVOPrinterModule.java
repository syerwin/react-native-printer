package com.pinmi.react.printer;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.pinmi.react.printer.adapter.UROVOPrinterDevice;
import com.pinmi.react.printer.adapter.UROVOPrinterAdapter;
import com.pinmi.react.printer.adapter.UROVOPrinterDeviceId;
import com.pinmi.react.printer.adapter.PrinterAdapter;

/**
 * Created by xiesubin on 2017/9/22.
 */

public class RNUROVOPrinterModule extends ReactContextBaseJavaModule implements RNPrinterModule {

    private PrinterAdapter adapter;
    private ReactApplicationContext reactContext;

    public RNUROVOPrinterModule(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }

    @ReactMethod
    @Override
    public void init(Callback successCallback, Callback errorCallback) {
        this.adapter = UROVOPrinterAdapter.getInstance();
        this.adapter.init(reactContext,  successCallback, errorCallback);
    }

    @ReactMethod
    @Override
    public void closeConn() {
        this.adapter.closeConnectionIfExists();
    }

    @ReactMethod
    @Override
    public void getDeviceList(Callback successCallback, Callback errorCallback) {
        this.adapter.getDeviceList(errorCallback);
    }

    @ReactMethod
    public void connectPrinter(Callback successCallback, Callback errorCallback) {
//        adapter.selectDevice(UROVOPrinterDeviceId.valueOf(), successCallback, errorCallback);
    }

    @ReactMethod
    @Override
    public void printRawData(String base64Data, Callback errorCallback) {
        adapter.printRawData(base64Data, errorCallback);
    }

    @ReactMethod
    @Override
    public void printImageData(String imagePath, Callback errorCallback){
//        adapter.printImageData(imagePath, errorCallback);
    }

    @ReactMethod
    @Override
    public void openCashDrawer(Callback errorCallback) {
//        adapter.openCashDrawer(errorCallback);
    }

    @Override
    public String getName() {
        return "RNUROVOPrinterModule";
    }
}
