package com.pinmi.react.printer.adapter;

import android.content.Context;
import android.device.DeviceManager;
import android.device.PrinterManager;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class UROVOPrinterAdapter implements PrinterAdapter{

    private Context mContext;
    private String LOG_TAG = "UROVOPrinterAdapter";
    private static UROVOPrinterAdapter mInstance = null;
    private PrinterManager mPrinter = null;
    private DeviceManager mDeviceManager = null;
    private UROVOPrinterDevice mUrovoPrinterDevice;

    private class TextToPrint {
        public String text;
        public int fontSize;
        public boolean isBold;
        public boolean isItalic;

        public TextToPrint(String text, int fontSize, boolean isBold, boolean isItalic) {
            this.text = text;
            this.fontSize = fontSize;
            this.isBold = isBold;
            this.isItalic = isItalic;
        }
    }

    private List<TextToPrint> textsToPrint;

    private int paperWidthSize = 384;

    private final int BETWEEN_LINE_SIZE = 5;
    private final String PRINT_FONT_NAME = "simsun";


    public UROVOPrinterAdapter(){
        mPrinter = new PrinterManager();
        mDeviceManager = new DeviceManager();
    }

    public static UROVOPrinterAdapter getInstance(){
        if(mInstance == null){
            mInstance = new UROVOPrinterAdapter();
        }
        return mInstance;
    }

    public int getPaperWidthSize() {
        return paperWidthSize;
    }

    public void setPaperWidthSize(int paperWidthSize) {
        this.paperWidthSize = paperWidthSize;
    }

    public int addRawText(String texts, int fontSize, boolean isBold, boolean isItalic){
        List<String> parseRawText = new ArrayList<>(Arrays.asList(texts.split("\n")));
        for (String text: parseRawText) {
            this.textsToPrint.add(new TextToPrint(text,fontSize,isBold,isItalic));
        }
        return this.textsToPrint.size();
    }

    public int addTextLine(String text, int fontSize, boolean isBold, boolean isItalic){
        this.textsToPrint.add(new TextToPrint(text,fontSize,isBold,isItalic));
        return this.textsToPrint.size();
    }

    public void clearTextArray(){
        this.textsToPrint.clear();
    }

    public int calculatePrintHeight(){
        int heightSum = 0;
        for (TextToPrint textLine: this.textsToPrint) {
            int height = textLine.fontSize + this.BETWEEN_LINE_SIZE;
            heightSum += height;
        }
        return heightSum;
    }

    public void prepareForPrint(){
        int paperHeightSize = calculatePrintHeight();
        int currentHeight = 0;
        this.mPrinter.prn_setupPage(this.paperWidthSize,paperHeightSize);

        for (TextToPrint textLine: this.textsToPrint) {
            mPrinter.drawText(textLine.text,0,currentHeight,this.PRINT_FONT_NAME,textLine.fontSize,textLine.isBold,textLine.isItalic,0);
            currentHeight += textLine.fontSize + this.BETWEEN_LINE_SIZE;
        }

    }

    public int beginPrint(){

        return this.mPrinter.prn_printPage(0);
    }

    @Override
    public void init(ReactApplicationContext reactContext, Callback successCallback, Callback errorCallback) {
        this.mContext = reactContext;
        this.textsToPrint.clear();
        successCallback.invoke();
    }

    @Override
    public List<PrinterDevice> getDeviceList(Callback errorCallback) {
        // errorCallback.invoke("do not need to invoke get device list for net printer");
        List<PrinterDevice> deviceList = new ArrayList<PrinterDevice>();
        
        if(mDeviceManager != null){
            deviceList.add(new UROVOPrinterDevice(mDeviceManager.getDeviceId()));
        }

        return deviceList;
        
    }

    @Override
    public void selectDevice(PrinterDeviceId printerDeviceId, Callback successCallback, Callback errorCallback) {

    }

    @Override
    public void closeConnectionIfExists() {

    }

    @Override
    public void printRawData(String rawBase64Data, Callback errorCallback) {

        if(mPrinter == null && mPrinter.getStatus() != 1){

            errorCallback.invoke("Printer not ready");
            return;

        }

        final Callback finErrorCallback = errorCallback;

        addRawText(rawBase64Data,24,false,false);
        calculatePrintHeight();
        prepareForPrint();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    int res = beginPrint();

                    if(res != 1){
                        finErrorCallback.invoke("Failed to print data");
                    }
                }catch (Exception e){
                    Log.e(LOG_TAG, "failed to print data");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void printImageData(String imagePath, Callback errorCallback) {

    }

    @Override
    public void openCashDrawer(Callback errorCallback) {

    }
}