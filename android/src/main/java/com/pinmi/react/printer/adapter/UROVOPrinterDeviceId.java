package com.pinmi.react.printer.adapter;

/**
 * Created by Tjen Syerwin on 2020/3/15.
 */

public class UROVOPrinterDeviceId extends PrinterDeviceId {

    private String deviceId;

    public String getDeviceId() {
        return this.deviceId;
    }


    public static UROVOPrinterDeviceId valueOf(String deviceId) {
        return new UROVOPrinterDeviceId(deviceId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UROVOPrinterDeviceId that = (UROVOPrinterDeviceId) o;

        if (!deviceId.equals(that.deviceId)) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = deviceId.hashCode();
        result = 31 * result;
        return result;
    }

    private UROVOPrinterDeviceId(String deviceId){
        this.deviceId = deviceId;
    }
}
