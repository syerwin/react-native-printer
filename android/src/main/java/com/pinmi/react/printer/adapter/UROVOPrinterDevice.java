package com.pinmi.react.printer.adapter;


import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

/**
 * Created by Tjen Syerwin on 2020/3/15.
 */

public class UROVOPrinterDevice implements PrinterDevice{
    private UROVOPrinterDeviceId urovoPrinterDeviceId;

    public UROVOPrinterDevice(String deviceId) {
        this.urovoPrinterDeviceId = UROVOPrinterDeviceId.valueOf(deviceId);
    }
    @Override
    public PrinterDeviceId getPrinterDeviceId() {
        return this.urovoPrinterDeviceId;
    }

    @Override
    public WritableMap toRNWritableMap() {
        WritableMap deviceMap = Arguments.createMap();
        deviceMap.putString("device_name", "urovo");
        deviceMap.putString("device_id", this.urovoPrinterDeviceId.getDeviceId());
        return deviceMap;
    }

}
